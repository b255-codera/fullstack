let collection = [];

// Write the queue functions below.
const print = () => {
    return collection;
}

const enqueue = (name) => {
    collection.push(name);
    return collection;
}

const dequeue = (name) => {
    collection.shift(name);
    return collection;
}

const front = () => {
    return collection[0];
}

const size = () => {
    return collection.length;
}

const isEmpty = () => {
    if(collection.length > 0) {
        return false;
    } else {
        return true;
    }
}

 
module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};